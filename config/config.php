<?php
/**
 * Created by PhpStorm.
 * User: nicoschneider
 * Date: 28/12/14
 * Time: 19:13
 */

/**
 * -------------------------------------------------------------------------
 * HOOKS
 * -------------------------------------------------------------------------
 */

$GLOBALS['TL_HOOKS']['processFormData'][] = array('Westwerk\EventSubscribe\Formdata\Processor', 'processSubmission');
$GLOBALS['TL_HOOKS']['replaceInsertTags'][] = array('Westwerk\EventSubscribe\Inserttag\InsertFormEvent', 'processTag');
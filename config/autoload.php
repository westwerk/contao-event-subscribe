<?php
/**
 * Created by PhpStorm.
 * User: nicoschneider
 * Date: 28/12/14
 * Time: 18:37
 */

ClassLoader::addClasses(array(
    'Westwerk\EventSubscribe\DCA\TlCalendarEvents' => 'system/modules/event-subscribe/src/Westwerk/EventSubscribe/DCA/TlCalendarEvents.php',
    'Westwerk\EventSubscribe\Formdata\Processor' => 'system/modules/event-subscribe/src/Westwerk/EventSubscribe/Formdata/Processor.php',
    'Westwerk\EventSubscribe\Formdata\EventEmail' => 'system/modules/event-subscribe/src/Westwerk/EventSubscribe/Formdata/EventEmail.php',
    'Westwerk\EventSubscribe\Formdata\EventEmailDeliveryException' => 'system/modules/event-subscribe/src/Westwerk/EventSubscribe/Formdata/EventEmailDeliveryException.php',
    'Westwerk\EventSubscribe\Formdata\FormDataProcessingException' => 'system/modules/event-subscribe/src/Westwerk/EventSubscribe/Formdata/FormDataProcessingException.php',
    'Westwerk\EventSubscribe\Inserttag\InsertFormEvent' => 'system/modules/event-subscribe/src/Westwerk/EventSubscribe/Inserttag/InsertFormEvent.php',
));

<?php
/**
 * Created by PhpStorm.
 * User: nicoschneider
 * Date: 28/12/14
 * Time: 17:22
 */

//<editor-fold desc="Palettes Configuration">

$GLOBALS['TL_DCA']['tl_calendar_events']['palettes']['default'] .= ';{event_subscribe_legend},enable_subscriptions';

$GLOBALS['TL_DCA']['tl_calendar_events']['palettes']['__selector__'] = array_merge(
    $GLOBALS['TL_DCA']['tl_calendar_events']['palettes']['__selector__'],
    array('enable_subscriptions')
);

$GLOBALS['TL_DCA']['tl_calendar_events']['subpalettes'] = array_merge(
    $GLOBALS['TL_DCA']['tl_calendar_events']['subpalettes'],
    array('enable_subscriptions' => 'subscription_form,subscriber_subject,subscription_email_sender,subscriber_recipient_field,subscriber_body,internal_subject,internal_recipient,internal_body')
);

//</editor-fold>

$GLOBALS['TL_DCA']['tl_calendar_events']['fields']['enable_subscriptions'] = array(
    'label'     => &$GLOBALS['TL_LANG']['tl_calendar_events']['enable_subscriptions'],
    'exclude'   => true,
    'inputType' => 'checkbox',
    'eval'      => array('tl_class' => 'w50 clr m12', 'submitOnChange' => true),
    'sql'       => "char(1) NOT NULL default ''",
);

$GLOBALS['TL_DCA']['tl_calendar_events']['fields']['subscription_form'] = array(
    'label'            => &$GLOBALS['TL_LANG']['tl_calendar_events']['subscription_form'],
    'exclude'          => true,
    'inputType'        => 'select',
    'options_callback' => array('Westwerk\EventSubscribe\DCA\TlCalendarEvents', 'getForms'),
    'eval'             => array('submitOnChange' => false, 'includeBlankOption' => false, 'tl_class' => 'w50'),
    'sql'              => "int(10) NULL",
);

//<editor-fold desc="Subscriber Email">

$GLOBALS['TL_DCA']['tl_calendar_events']['fields']['subscriber_subject'] = array(
    'label'     => &$GLOBALS['TL_LANG']['tl_calendar_events']['subscriber_subject'],
    'exclude'   => true,
    'inputType' => 'text',
    'eval'      => array('tl_class' => 'w50 clr'),
    'sql'       => "varchar(1024) NOT NULL default ''",
);

$GLOBALS['TL_DCA']['tl_calendar_events']['fields']['subscription_email_sender'] = array(
    'label'     => &$GLOBALS['TL_LANG']['tl_calendar_events']['subscription_email_sender'],
    'exclude'   => true,
    'inputType' => 'text',
    'eval'      => array('tl_class' => 'w50'),
    'sql'       => "varchar(1024) NOT NULL default ''",
);

$GLOBALS['TL_DCA']['tl_calendar_events']['fields']['subscriber_body'] = array(
    'label'     => &$GLOBALS['TL_LANG']['tl_calendar_events']['subscriber_body'],
    'exclude'   => true,
    'search'    => true,
    'inputType' => 'textarea',
    'eval'      => array(
        'rte'      => 'tinyMCE',
        'tl_class' => 'clr',
        'style'    => 'height: 100px;'
    ),
    'sql'       => "text NOT NULL",
);

//</editor-fold>

//<editor-fold desc="Internal Email">

$GLOBALS['TL_DCA']['tl_calendar_events']['fields']['internal_subject'] = array(
    'label'     => &$GLOBALS['TL_LANG']['tl_calendar_events']['internal_subject'],
    'exclude'   => true,
    'inputType' => 'text',
    'eval'      => array('tl_class' => 'w50 clr'),
    'sql'       => "varchar(1024) NOT NULL default ''",
);

$GLOBALS['TL_DCA']['tl_calendar_events']['fields']['internal_recipient'] = array(
    'label'     => &$GLOBALS['TL_LANG']['tl_calendar_events']['internal_recipient'],
    'exclude'   => true,
    'inputType' => 'text',
    'eval'      => array('tl_class' => 'w50'),
    'sql'       => "varchar(1024) NOT NULL default ''",
);

$GLOBALS['TL_DCA']['tl_calendar_events']['fields']['internal_body'] = array(
    'label'     => &$GLOBALS['TL_LANG']['tl_calendar_events']['internal_body'],
    'exclude'   => true,
    'search'    => true,
    'inputType' => 'textarea',
    'eval'      => array(
        'rte'      => 'tinyMCE',
        'tl_class' => 'clr',
        'style'    => 'height: 100px;'
    ),
    'sql'       => "text NOT NULL",
);

//</editor-fold>
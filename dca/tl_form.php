<?php
/**
 * Created by PhpStorm.
 * User: nicoschneider
 * Date: 14/01/15
 * Time: 15:22
 */

$GLOBALS['TL_DCA']['tl_form']['palettes']['default'] .= ';{event_subscribe_legend},subscriber_recipient_field';

$GLOBALS['TL_DCA']['tl_form']['fields']['subscriber_recipient_field'] = array(
    'label'            => &$GLOBALS['TL_LANG']['tl_form']['subscriber_recipient_field'],
    'explanation'      => &$GLOBALS['TL_LANG']['tl_form']['subscriber_recipient_field_help'],
    'exclude'          => true,
    'inputType'        => 'select',
    'options_callback' => array('Westwerk\EventSubscribe\DCA\TlCalendarEvents', 'getFields'),
    'eval'             => array(
        'submitOnChange' => false,
        'includeBlankOption' => false,
        'tl_class' => 'w50'
    ),
    'sql'              => "varchar(1024) NOT NULL default ''",
);
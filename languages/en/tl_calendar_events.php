<?php
/**
 * Created by PhpStorm.
 * User: nicoschneider
 * Date: 28/12/14
 * Time: 17:27
 */

$GLOBALS['TL_LANG']['tl_calendar_events']['event_subscribe_legend'] = 'Event Subscriptions';

$GLOBALS['TL_LANG']['tl_calendar_events']['enable_subscriptions'] = array(
    'Enable subscriptions',
    'Check this field to enable subscriptions for this event.'
);

$GLOBALS['TL_LANG']['tl_calendar_events']['subscriber_subject'] = array(
    'Email subject (to subscriber)',
    'This will be used as the subject for the email to the subscriber.'

);

$GLOBALS['TL_LANG']['tl_calendar_events']['subscriber_body'] = array(
    'Email to subscriber',
    'This will be used as the body for the email to the subscriber.'
);

$GLOBALS['TL_LANG']['tl_calendar_events']['internal_subject'] = array(
    'Email subject (internal)',
    'This will be used as the subject for the internal email.'
);

$GLOBALS['TL_LANG']['tl_calendar_events']['internal_recipient'] = array(
    'Internal email recipient',
    'The internal confirmation email will go to this address.'
);

$GLOBALS['TL_LANG']['tl_calendar_events']['internal_body'] = array(
    'Internal email',
    'This will be used as the body for the internal email.'
);

$GLOBALS['TL_LANG']['tl_calendar_events']['subscription_form'] = array(
    'Subscription form',
    'Choose a form from the Form Generator with which subscriptions will be made.'
);

$GLOBALS['TL_LANG']['tl_calendar_events']['subscriber_recipient_field'] = array(
    'Recipient field',
    'Choose a field from your form to which the subscriber email will be sent. <strong>Be careful:</strong> You must select a field from the form you selected above!'
);

$GLOBALS['TL_LANG']['tl_form']['subscriber_recipient_field'] = array(
    'Recipient field',
    'Choose a field from your form to which the subscriber email will be sent. <strong>Be careful:</strong> You must select a field from the form you selected above!'
);

$GLOBALS['TL_LANG']['tl_calendar_events']['subscriber_recipient_field_help'] = 'Test';
<?php
/**
 * Created by PhpStorm.
 * User: nicoschneider
 * Date: 28/12/14
 * Time: 17:27
 */

$GLOBALS['TL_LANG']['tl_form']['event_subscribe_legend'] = 'Event Subscriptions';

$GLOBALS['TL_LANG']['tl_form']['subscriber_recipient_field'] = array(
    'Recipient field',
    'If this form is used in an event for subscriptions, this field wil supply the recipient of confirmation emails.'
);
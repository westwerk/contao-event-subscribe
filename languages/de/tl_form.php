<?php
/**
 * Created by PhpStorm.
 * User: nicoschneider
 * Date: 28/12/14
 * Time: 17:27
 */

$GLOBALS['TL_LANG']['tl_form']['event_subscribe_legend'] = 'Anmeldungen';

$GLOBALS['TL_LANG']['tl_form']['subscriber_recipient_field'] = array(
    'Empfänger-Feld',
    'Falls dieses Formular in einem Event für Anmeldungen verwendet wird, wird dieses Feld als Empfänger für Bestätigungs-Emails verwendet.'
);
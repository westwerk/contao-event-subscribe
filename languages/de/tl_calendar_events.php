<?php
/**
 * Created by PhpStorm.
 * User: nicoschneider
 * Date: 28/12/14
 * Time: 17:27
 */

$GLOBALS['TL_LANG']['tl_calendar_events']['event_subscribe_legend'] = 'Anmeldungen';

$GLOBALS['TL_LANG']['tl_calendar_events']['enable_subscriptions'] = array(
    'Für Anmeldungen freischalten',
    'Setzen Sie hier einen Haken, um dieses Event für Anmeldungen freizuschalten.'
);

$GLOBALS['TL_LANG']['tl_calendar_events']['subscriber_subject'] = array(
    'Email-Betreff (an den Anmelder)',
    'Betreffzeile für die Bestätigungsemail an den Anmelder.'
);

$GLOBALS['TL_LANG']['tl_calendar_events']['subscription_email_sender'] = array(
    'Email-Absender (beide Emails)',
    'Format: "max@muster.mann" oder "max@muster.mann|Max Mustermann"'
);

$GLOBALS['TL_LANG']['tl_calendar_events']['subscriber_body'] = array(
    'Email an den Anmelder',
    'Text der Email an den Anmelder. Alle Tags aus dem EFG werden unterstützt.'
);

$GLOBALS['TL_LANG']['tl_calendar_events']['internal_subject'] = array(
    'Email-Betreff (intern)',
    'Betreffzeile für die interne Email-Benachrichtigung..'
);

$GLOBALS['TL_LANG']['tl_calendar_events']['internal_recipient'] = array(
    'Empfänger der internen Email',
    'Die interne Email-Benachrichtigung wird an diese Adresse gesendet.'
);

$GLOBALS['TL_LANG']['tl_calendar_events']['internal_body'] = array(
    'Interne Email',
    'Text der internen Email. Alle Tags aus dem EFG werden unterstützt.'
);

$GLOBALS['TL_LANG']['tl_calendar_events']['subscription_form'] = array(
    'Anmeldungs-Formular',
    'Wählen Sie ein Formular aus dem Formulargenerator aus, mit dem sich für dieses Event angemeldet werden kann.'
);

$GLOBALS['TL_LANG']['tl_calendar_events']['subscriber_recipient_field'] = array(
    'Empfänger-Feld',
    'Wählen Sie ein Feld aus Ihrem Formular aus, in welchem die Email-Adresse des Empfängers der Benachrichtigungs-Email eingetragen wird. Sie <strong>müssen</strong> ein Feld aus demselben Formular auswählen, welches Sie für dieses Event ausgewählt haben.'
);

$GLOBALS['TL_LANG']['tl_form']['subscriber_recipient_field'] = array(
    'Empfänger-Feld',
    'Wählen Sie ein Feld aus Ihrem Formular aus, in welchem die Email-Adresse des Empfängers der Benachrichtigungs-Email eingetragen wird. Sie <strong>müssen</strong> ein Feld aus demselben Formular auswählen, welches Sie für dieses Event ausgewählt haben.'
);

$GLOBALS['TL_LANG']['tl_calendar_events']['subscriber_recipient_field_help'] = 'Test';
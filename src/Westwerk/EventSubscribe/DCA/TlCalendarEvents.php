<?php
/**
 * Created by PhpStorm.
 * User: nicoschneider
 * Date: 28/12/14
 * Time: 18:35
 */

namespace Westwerk\EventSubscribe\DCA;

use Westwerk\EventSubscribe\Helper\Database;

class TlCalendarEvents extends \Backend {

    public function getForms() {

        $results = $this->Database
            ->prepare('SELECT * FROM tl_form')
            ->execute();

        if($results->numRows < 1) {
            return array();
        }

        $results = $results->fetchAllAssoc();

        $return = array();

        foreach($results as $result) {
            $title = trim($result['title']);
            $return[$result['id']] = $title . ' (ID ' . $result['id'] . ')';
        }

        return $return;
    }

    public function getFields() {

        // check if we are editing a form, and if so
        // just get its fields instead of all the fields
        if($_GET['do'] == 'form'
            && $_GET['act'] == 'edit') {
            $formId = $_GET['id'];
            return $this->getFieldsByFormId($formId);
        }

        $forms = $this->getForms();
        $return = array();
        foreach($forms as $formId => $formLabel) {
            $return[$formLabel] = $this->getFieldsByFormId($formId);
        }
        return $return;
    }

    private function getFieldsByFormId($formId) {
        $results = $this->Database
            ->prepare('SELECT * FROM tl_form_field WHERE pid = ?')
            ->execute($formId)
            ->fetchAllAssoc();

        $fields = array();

        foreach($results as $result) {
            $fields[$result['name']] = $result['label'] . ' (ID ' . $result['id'] . ')';
        }

        return $fields;
    }
}
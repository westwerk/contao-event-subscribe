<?php
/**
 * Created by PhpStorm.
 * User: nicoschneider
 * Date: 02/03/15
 * Time: 10:25
 */

namespace Westwerk\EventSubscribe\Inserttag;

use Contao\Controller;
use Westwerk\EventSubscribe\Formdata\Processor;

/**
 * Class InsertFormEvent
 *
 * We extend Contao\Controller here to get to the getForm
 * method, which renders a given form id into a string
 * which we can work this. Feels a little hacky, though.
 *
 * @package Westwerk\EventSubscribe\Inserttag
 */
class InsertFormEvent extends Controller
{

    /**
     * Processes the replacement of insert_event_form::x::y. This
     * gets called from Contao's replaceInsertTags hook
     *
     * @param $tag
     */
    public function processTag($tag)
    {

        $segments = explode("::", $tag);

        if ($segments[0] != "insert_event_form") {
            return '';
        }

        if (count($segments) != 3) {
            throw new \InvalidArgumentException("The insert_event_form tag needs to be in the form of {{insert_event_form::FORM_ID::EVENT_ID}}");
        }

        // let Contao do the form magic
        $formHtml = $this->getForm($segments[1]);

        // hackily inject a hidden field with the event id
        $formHtml = $this->injectEventId($segments[2], $formHtml);

        // hackily overwrite a field named "event_full_name"
        // with a hidden field containing the event name
        // (if we can)
        $event = $this->getEventById($segments[2]);

        if(isset($event[0]) && isset($event[0]['title'])) {
            $formHtml = $this->injectEventName($event[0]['title'], $formHtml);
        }

        return $formHtml;
    }

    /**
     * Uses the Processor's event getter to get
     * the event data.
     *
     * @param $eventId int|string
     * @return null
     */
    private function getEventById($eventId)
    {
        $processor = new Processor();
        return $processor->getEventById($eventId);
    }

    /**
     * Injects a hidden "event_subscribe_event_id" field after
     * the opening <form> tag which contains the given event id.
     *
     * @param $eventId int|string
     * @param $formHtml string
     * @return string
     */
    public function injectEventId($eventId, $formHtml)
    {

        $hiddenEventIdInput = sprintf(
            '<input type="hidden" name="event_subscribe_event_id" value="%1$s" />',
            $eventId
        );

        return preg_replace("/(<form.+>)/", "$1" . $hiddenEventIdInput, $formHtml);
    }

    /**
     * Replaces a hidden field named 'event_full_name' to make referencing
     * form submissions in Conato possible (hacky).
     *
     * @param $eventName string
     * @param $formHtml string
     * @return string
     */
    public function injectEventName($eventName, $formHtml)
    {

        $pattern = '/<input[a-zA-Z\_\-\s\d="]*name="event_full_name"[a-zA-Z\_\-\s\d="]*>/i';

        if (!preg_match($pattern, $formHtml)) {
            return $formHtml;
        }

        $hiddenEventNameInput = sprintf(
            '<input type="hidden" name="event_full_name" value="%s" />',
            $eventName
        );

        return preg_replace($pattern, $hiddenEventNameInput, $formHtml);
    }

}
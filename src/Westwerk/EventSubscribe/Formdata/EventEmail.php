<?php
/**
 * Created by PhpStorm.
 * User: nicoschneider
 * Date: 29/12/14
 * Time: 15:51
 */

namespace Westwerk\EventSubscribe\Formdata;

class EventEmail extends \Email {

    /**
     * @var array
     */
    protected $recipients = array();

    /**
     * @return array
     */
    public function getRecipients() {

        return $this->recipients;
    }

    /**
     * @param array $recipients
     * @return $this
     */
    public function setRecipients(array $recipients) {

        $this->recipients = $recipients;

        return $this;
    }

    /**
     * @param $recipient
     * @return $this
     */
    public function addRecipient($recipient) {

        $this->recipients[] = $recipient;

        return $this;
    }
}
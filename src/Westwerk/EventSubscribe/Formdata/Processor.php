<?php
/**
 * Created by PhpStorm.
 * User: nicoschneider
 * Date: 28/12/14
 * Time: 19:12
 */

namespace Westwerk\EventSubscribe\Formdata;

class Processor extends \Frontend {

    /**
     * This method gets called by Contao's event hook system
     * and processes all events that have selected this form
     *
     * @param      $submission
     * @param null $form
     * @param null $files
     * @param null $labels
     */
    public function processSubmission($submission, $form = null, $files = null, $labels = null) {

        // Determine whether this form was selected by at least one event
        if(!$this->formHasEvents($form['id'])) {
            return;
        }

        // See if we have a event id passed, if so,
        // use only this event, otherwise get all events
        // associated with this form (dubious?)
        if(!empty($submission['event_subscribe_event_id'])) {
            $events = $this->getEventById($submission['event_subscribe_event_id']);
        } else {
            $events = $this->getEventsByFormId($form['id']);
        }

        // Iterate over all events with this form and
        // process them individually
        foreach($events as $event) {
            try {
                $this->processSingleEvent($event, $submission, $form);
            } catch(FormdataProcessingException $e) {
                $this->log($e->getMessage(), 'Westwerk\Eventsubscribe\Formdata\Processor::processSubmission', 'warning');
            }
        }
    }

    /**
     * This method takes care of sending out emails for a
     * single event
     *
     * @param null $event
     * @param null $submission
     * @param null $form
     */
    private function processSingleEvent($event = null, $submission = null, $form = null) {

        if(null === $event
            || null === $form
            || null === $submission
        ) {
            throw new FormdataProcessingException("Malformed data in formdata processing event hook!");
        }

        $emails = $this->prepareEmails($event, $submission, $form);

        foreach($emails as $email) {
            /**@var EventEmail $email */
            if(!$email->sendTo($email->getRecipients())) {
                throw new EventEmailDeliveryException("Cannot send event email!");
            }
        }
    }

    /**
     * Get all events that have the given form selected with enabled
     * subscriptions
     *
     * @param null $formId
     * @return null
     */
    private function getEventsByFormId($formId = null) {

        if(null === $formId) {
            return null;
        }

        $results = $this->Database
            ->prepare('SELECT * FROM tl_calendar_events WHERE subscription_form = ? AND enable_subscriptions = 1')
            ->execute($formId);

        if(!$results->numRows > 0) {
            return null;
        }

        return $results->fetchAllAssoc();
    }

    public function getEventById($eventId = null) {

        if(null === $eventId) {
            return null;
        }

        $results = $this->Database
            ->prepare('SELECT * FROM tl_calendar_events WHERE id = ? AND enable_subscriptions = 1')
            ->execute($eventId);

        if(!$results->numRows > 0) {
            return null;
        }

        return $results->fetchAllAssoc();

    }

    /**
     * Helper method to determine whether a form has events
     * attached
     *
     * @param null $formId
     * @return bool
     */
    private function formHasEvents($formId = null) {

        if(null === $formId) {
            return false;
        }

        $events = $this->getEventsByFormId($formId);

        return (count($events) > 0);
    }

    /**
     * Prepares the internal and confirmation emails
     *
     * @param $event
     * @param $submission
     * @param $form
     * @return EventEmail[]
     */
    private function prepareEmails($event, $submission, $form) {

        $this->import('FormdataProcessor');

        // we're expecting either something like
        // 'max@muster.mann' or 'max@muster.mann|Max Mustermann'
        // (without the quotes)
        if(!empty($event['subscription_email_sender'])) {
            $from = trim($event['subscription_email_sender']);
            $fromParts = explode('|', $from);
        }

        // 1. Internal Email
        $internalEmail = new EventEmail();

        // Set from and fromName if they have been set
        if(isset($fromParts[0])) {
            $internalEmail->from = $fromParts[0];
        }

        if(isset($fromParts[1])) {
            $internalEmail->fromName = $fromParts[1];
        }

        // @todo: replace tags here as well
        $internalEmail->subject = $event['internal_subject'];

        // try to explode a comma-separated list of recipients
        $internalRecipients = explode(",", $event['internal_recipient']);
        foreach($internalRecipients as $internalRecipient) {
            $internalRecipient = trim($internalRecipient);
            $internalEmail->addRecipient($internalRecipient);
        }

        // We have to set this so we can use EFG's tag replacement magic
        $_SESSION['EFP']['FORM_DATA']['_formId_'] = $form['id'];

        // Also, we need to surround the content with a <body> tag for this to work
        $internalBody = sprintf('<body>%1$s</body>', $event['internal_body']);

        // Get those tags replaced!
        $internalBody = $this->replaceEventTags($internalBody, $event);
        $internalBody = $this->FormdataProcessor->processConfirmationContent($internalBody);

        // Remove the body tag again
        $internalEmail->html = str_replace(array('<body>', '</body>'), array(), $internalBody);

        // 2. Prepare subscriberEmail, 'baby I can do this often, often!'
        $subscriberEmail = new EventEmail();

        // Set from and fromName if they have been set
        if(isset($fromParts[0])) {
            $subscriberEmail->from = $fromParts[0];
        }

        if(isset($fromParts[1])) {
            $subscriberEmail->fromName = $fromParts[1];
        }

        // @todo: replace tags here as well
        $subscriberEmail->subject = $event['subscriber_subject'];

        $subscriberBody = sprintf('<body>%1$s</body>', $event['subscriber_body']);

        // Get those tags replaced!
        $subscriberBody = $this->replaceEventTags($subscriberBody, $event);
        $subscriberBody = $this->FormdataProcessor->processConfirmationContent($subscriberBody);

        // Remove the body tag again
        $subscriberEmail->html = str_replace(array('<body>', '</body>'), array(), $subscriberBody);

        // if the Form which was selected in the Event has no recipient
        // field set, we can't send the confirmation email
        if(!empty($form['subscriber_recipient_field'])) {
            // get the recipient's address from the field that was set
            // in the form options ("Recipient Field")
            $subscriberEmail->addRecipient($submission[$form['subscriber_recipient_field']]);

            // done, return both of the email objects!
            return array($internalEmail, $subscriberEmail);
        } else {
            // we'll just return the internal email
            return array($internalEmail);
        }
    }

    /**
     * @param $subject string
     * @param $eventData array
     * @return string
     */
    public function replaceEventTags($subject, $eventData) {

        $pattern = "/\{\{\bevent\b\:\:([\_a-zA-Z]*)\}\}/i";

        $timeFields = array('startTime', 'endTime');
        $dateFields = array('startDate', 'endDate');

        $matches = array();
        preg_match_all($pattern, $subject, $matches);

        foreach($matches[0] as $index => $match) {
            $tag = $matches[1][$index];
            if(isset($eventData[$tag])) {
                $newValue = $eventData[$tag];

                if(in_array($tag, $timeFields)) {
                    $newValue = date($GLOBALS['TL_CONFIG']['timeFormat'], $newValue);
                } elseif(in_array($tag, $dateFields)) {
                    $newValue = date($GLOBALS['TL_CONFIG']['dateFormat'], $newValue);
                }

                $subject = str_replace($match, $newValue, $subject);
            }
        }

        return $subject;
    }
}
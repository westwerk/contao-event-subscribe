# Contao Event Subscribe

* Version 1.1.8
* Contao 3.2 LTS extension
* Allows subscriptions to Events.

## Installation

This extension is compatible with Contao 3.2 LTS.

### With the Composer Extension

* Install `westwerk/contao-event-subscribe`

### Manual

* You need the _Extended Form Generator_ extension (`contao-legacy/efg` for Composer people)
* Drop the contents of this repository in a folder named `event-subscribe`.
* Update your database (either via the Install Tool or Package Management, if you happen to use the Composer extension - which you should).

## Usage

1. Create a signup form for your event. It has to contain a field in which visitors must enter a email address.
2. Create an event and check _Enable subscriptions_.
3. Choose your form
4. The field you want the confirmation email to be sent to *has to be chosen in the form!*
5. For the email body fields, you can use Contao's Insert Tags as well as `{{form::}}` tags, just like in EFG. This extension also supports EFG's conditional tags.

### Adding a subscription form to the event detail template

You can use something along the lines of `{{insert_form::<?= $this->subscription_form ?>}}` for your event detail template.

## Roadmap/Todo

* Parse the subject fields (`internal_subject`, `subscriber_subject`) for Insert Tags as well.
* Make this thing ready for the Composer extension.
* For the _Recipient field_, show ony form fields from the selected form.
